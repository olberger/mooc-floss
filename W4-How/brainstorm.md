Code of conducts:
* The social rules to interact, they set expectations
* The last resort when people have issues
* Provide examples: avoid de-anonymizable examples. Maybe mention the Linus case where he admitted that social issues *are* an issue
* "newcomers" can be easy "targets" for abusive behaviors, so it's important to recognize those situations.
* feedback about the atmosphere of a community is a contribution.

Burn outs
- Role transition in floss communities:  https://www.researchgate.net/profile/Patrick-Mukala/publication/313254065/figure/fig1/AS:961423358304278@1606232599177/Role-transition-in-FLOSS-communities-From-Glott-et-al-2011_W640.jpg

Retention in projects:
* https://ieeexplore.ieee.org/abstract/document/8811892?casa_token=A79_i5pOZ1IAAAAA:QLO11ijN2Riu1qoRjAHivemeffo1FgcjfEQ3ncBkbHjTPZwlKROwg93JVOVot9xpIiFFAXA
* Some factors which affect retention cannot be controlled: the popularity of the project, and how early in the life-cycle a developer joins [8], [9]. However, there are also measures that communities can take to encourage retention. For example, modular code and early social interactions with peers are both associated with retention [8], [10]

Polyedre:Do you recommend starting contributing in a big community or in a small one ? does it matter ?
=> big ones are *usually* easier to onboard (as they are more used to onboarding)

References:
- Workflow net for novice during initiation phase: https://www.researchgate.net/profile/Patrick-Mukala/publication/313254065/figure/fig3/AS:961423358304279@1606232599420/Workflow-net-for-novice-during-the-initiation-phase_W640.jpg
- https://www.researchgate.net/publication/41618542_Towards_a_theory_on_the_sustainability_and_performance_of_FLOSS_communities/figures?lo=1


Marc Jeanmougin:https://wiki.communitydata.science/Main_Page
Rémi SHARROCK:https://opensourcedesign.net/


