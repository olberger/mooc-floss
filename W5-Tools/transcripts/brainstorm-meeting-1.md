
S1: 00:00:03.566 Hi everyone and welcome to this brainstorming session number five-- no, six already because the first one-- no, five? I can't remember now. Yeah, number five.

S2: 00:00:14.293 Five, I thought. No, six because it starts from zero.

S3: 00:00:18.967 Six there was. Well, but, yeah, zero is introduction but we had one about the setup as well.

S1: 00:00:27.270 Yes, indeed. So it's about this wonderful project on how to contribute to FLOSS project, free, libre, and open-source projects. This brainstorming session is about something quite technical, advanced tooling. So everything around how to test a code, how to review a code, how to analyze a code, how to build from source code to make something executable. So there is a lot of tooling involved here and a lot of complexity sometimes. And so we are wondering how to make it possible, and that will be one of the most difficult and challenging parts in terms of technical difficulty for the students, I guess. So the transmission part, we could give some ideas on the basics from source to build. We will discuss about that.

S1: 00:01:43.293 Anyway, as you see here, the interaction in the forums could be-- ultimately how to share, how to mentor other students on how to build because I'm pretty sure the complexity will be huge for some of the projects that your students will want to contribute to. So help one another on the project will be the most intensive part during the interaction on this section.

S1: 00:02:23.103 And the activity we thought about was to reproduce a minor bug that has been found on the bug tracker of the project. But to do that, not only you have to reproduce the bug on the running application, but you have to take the source code of the application, build it with the build tools, and with the built one, reproduce the bug. I think this is the idea, right, Marc.

S4: 00:02:59.460 Yep. It's not just test stuff that has been reported on released version but really build something from source and check that the bug is still present, for instance, and run it.

S1: 00:03:13.897 Right, so maybe the built version is more advanced and has a lot of bug fixes. So it means that the bug has still to be opened in the bug tracker, and you have to reproduce it. So it means that you have to use the development version of the software. So maybe development version and release version, all this kind of taxonomy has to be introduced also.

S1: 00:03:45.862 All right. How to evaluate the work? We don't know. Maybe you write a test, but how can we evaluate that automatically? Should we use peer grading? But that's difficult also. So any idea is welcome I think. So that's a challenging section I think for today.

S4: 00:04:10.459 Yeah. And especially since we want some sort of language-agnostic MOOC because many tools are language specific or domain specific. If you build a desktop application, it will not be the same tools as if you try to run a web service. It will not be the same tools if you just try to run a Java JAR file, or Python website, Django website, or. There are many, many different topics. And People might not be interested in all of them, of course.

S4: 00:04:55.676 So I think it's important to convey the ideas that could be common to many or most of those. Maybe mentioned the domains, or. I'm not exactly sure how to make it interesting for-- I'm torn between presenting every standard stuff that they could encounter, regardless if it's used in their project or do a very modular design with basically something a la carte, where they could say, "Oh, I'm interested in testing in Python. And not at all in using Docker to host a web service on some ports because that's how our stuff is distributed in that project."

S2: 00:05:46.689 I think, if you dive into the specifics, it will be too big. And I also think that you can stay language and environment neutral while conveying something very useful. Because there always is one activity, for instance, fetching the code. So you can talk about that in generic terms. And you can have a peer-reviewed activity, that is, "Okay, fetch the code from your project, have it on your machine." That's the activity.

S2: 00:06:33.962 And then once you you've got the code, another agnostic activity is, how do you express a modification? How do you send it? So for instance, if you have this bizarre Phabricator suite and not pull requests with GitHub, it's going to be very different. But in the end, the activity will be, "Okay. I did something locally on my machine, and how do I make it to land so that the upstream sees it?" So you can express it in this way. And all the steps of the contribution that are technical steps, that involve technical tools, I think can be usefully expressed. And for each of them you can have an activity that cannot be measured by questions that are in the course but must be showed to someone else. But there is no need for expertise, for instance, to show that the code is on your machine. There is no need for expertise to see that a pull request was opened. So it can be peer reviewed probably.

S1: 00:08:03.404 Xavier was also mentioning cloud programming. So it could be also in the clouds, like Gitpod or Eclipse J or I don't know. Some of the projects could be directly cloned into an instance in the cloud with all the configuration.

S2: 00:08:28.831 But in the end, it must be a machine where you have some kind of control where you can do modifications. So, yeah, you're right. It's not necessary. Your laptop can be a machine that you control where your work environment is.

S1: 00:08:47.890 Right. Because even to add more to the complexity, each machine of the students will be different with a different operating system. And maybe the machine will be so slow, that sometimes if the codebase is so big, you cannot even manage to download everything. Yeah, so we have also to think about that.

S3: 00:09:23.652 Yeah-- sorry, go, Marc.

S4: 00:09:24.792 It will be hard to contribute if you cannot really build the code on your machine.

S1: 00:09:29.599 No, I don't agree. You can have very dumb, stupid machine with really few resources and do everything in the cloud.

S3: 00:09:44.257 I would point--

S5: 00:09:45.553 This is something that-- sorry. Yeah, go ahead.

S3: 00:09:46.313 No, sorry. Go, Ana.

S5: 00:09:48.859 No, you [inaudible] for me.

S3: 00:09:51.680 All right. I mean, my point is maybe more broad. So maybe it's better if you do it first. But it's about scope here. I'm wondering how far we want to go into that because that's going to change massively from one project to the next like we were discussing. And I especially feel-- I mean, if we were just focusing on one project, of course, that could be like an easy module. But because it's about any project, isn't that more the responsibility of the project themselves to explain how to set up the development environment? So maybe we could just explain what they need to be looking for, what they need to be asking for in the project, rather than trying to do that for the project.

S1: 00:10:42.638 We were talking about contributing guides, but is it standard that you have some kind of how to get a development version working on your computer, or [crosstalk]--?

S2: 00:11:04.352 If a project does not have that, then you cannot contribute to it. You just [no way?]. I mean, it's a given.

S1: 00:11:15.841 Okay. So a good contribution guide also has this explanation, right?

S2: 00:11:21.133 It can be just one page. But if it's not there, and if it's so difficult that you cannot figure it out, then it's non-starter.

S4: 00:11:31.976 Sometimes it's not explicit like, "This is a Django website project." So people are sort of supposed to know how to call Apache with how to run a Django site, and sometimes it's not explicit.

S2: 00:11:50.820 Even that I disagree. Django to run the tests, most of the time, it's completely non-trivial. And if it's not explained it can take you days to figure out because there are specifics. I know because three weeks ago, I ran into exactly this problem, and I had to document it. My first contribution was to document the set up of the test environment. I was paid to contribute so I went over the hoops anyways. But if I had been a contributor on my free time, I wouldn't have done it. It turned out to be very few lines in the end. But it was really difficult to figure out these three lines. And, Ana, you wanted to say something?

S5: 00:12:55.923 Yeah. I just wanted to double back to the topic that was discussed previously. If I'm understanding correctly, the contribution of this week will be dependent on the technical devices and the technical setup that the learners have. Am I right? So I just wanted to make a point that it's super important to anticipate that before so that the learners don't arrive at potential problem of this week. But that for example, their, how do I say, [foreign], the description of what the learner needs to do in order to pass this MOOC like the knowledge, the setup etc., they can see what technical setup they need to have in order to actually accomplish this part of the MOOC too.

S1: 00:13:45.129 I completely agree. So do you all agree that the very basic command line, Unix command line, Linux and stuff should be a prerequisite at least?

S2: 00:14:05.133 That's a very good question. What are the prerequisites?

S4: 00:14:18.383 I know some people are developing under Windows and don't really use the command line and just an IDE with everything under the hood. But in many cases, you can need the command line. In most cases, I know we'll need the command line.

S2: 00:14:36.562 Now maybe the command line. But there is for sure a prerequisite is you know what a diff is. You have to know what a diff is. You have to know what a DVCS is.

S1: 00:14:52.915 What is it?

S2: 00:14:55.466 Sorry what?

S1: 00:14:56.705 What is it? I don't know.

S2: 00:14:59.694 You cannot contribute. A versioning system. Git, or. A distributed versioning control system.

S1: 00:15:09.044 All right. All right. Yes I know.

S2: 00:15:11.200 So you have to know that kind of thing. You have to know the difference between a unit test and an integration test. And it goes to what you will be asked to provide with the fix. Most of the time, it's a unit test. And if you're faced with an integration test, you have to know that it's going to be very much difficult, much more difficult. But if you don't know the difference between the two, it's going to be a problem. So you have to know that too. So these are the kind of prerequisite. You have to be able to navigate the concepts that will be required of you, rather than specific tools.

S2: 00:16:09.318 If you think about the steps that involve technical tools and that you will have to use-- so there is the diff that you must send. There is the global method of knowing a DVCS, how it operates so that you can get the code to do the modification. I've seen people who don't know how to do that, and they are stuck with the web interface of GitLab or GitHub. And it's fine to fix one typo, but it's a nightmare when they want you to write a line of code. Oh yes, they also have to understand-- and this is paramount because you cannot learn that during the course. They have to understand how to rewrite the history of a series of commit. Let's say it's a soft requirement. If they don't know how to do that, they will not be able to fix the diff that they send. That is, they will have a lot of difficulties [crosstalk]--

S4: 00:17:28.631 Oh, to rebase?

S2: 00:17:30.799 Yes, to rebase, to modify the commits. Otherwise, they will end up adding a series of commit changes upon changes that do not look good for the review.

S1: 00:17:49.389 Interesting point. I was also thinking about if you find a bug in the bug tracker, maybe the bug is linked to specific part of the source code. It is linked to maybe existing merge request. There is a discussion. There is a lot of context around the bug, could be. So sometimes I have to navigate through the platform, GitHub, GitLab, Launchpad, I don't know, other things, to get in for around the bug and even get the place in the source code where the bug is supposed to happen.

S2: 00:18:38.228 Oh, yes. And that's true, so all the environment around the bug. But I don't know if you can place-- can you name the skill that is required to do that other than some common sense and the ability to explore, to navigate, to click? There is something else. It's that one skill that, again, it's maybe optional. It's the ability to read the successive modifications of a file because when you contribute, the most useful tool that you have is what has been done in the past around the lines that you're supposed to fix. So if you can look at the history, make sense of the history of the code, you are in a much better shape. And I would argue that when you face a bug that you don't author on a codebase that you don't know, it will speed up considerably the acceptance of your patch if you do it in the same kind of way that was done before. And for that, you have to be able to read the history of the modifications. Sorry. And this is a technical requirement. It's not something that is social in nature. You have to be able to read a succession of diffs.

S4: 00:20:20.066 I would [consider that?] something in the next week where you-- the next week is basically how to navigate in big codebases. And I would say that actually using the version control system is a very important part of that.

S1: 00:20:41.241 Every time we do a week of brainstorming, Marc, you always say that. You always say, "We will see that next week."

S4: 00:20:50.494 Well, I cannot say that the next week.

S1: 00:20:53.118 The whole MOOC is completely reversed. [laughter]

S2: 00:20:58.723 Marc, you said, "We will also need that next week," not, "We don't need that this week."

S4: 00:21:06.222 I say it would be more of a-- I thought of that as sort of a focus for next week, where I mentioned Git grab but of course, Git blame and Git log are some of the most important things you can use to navigate codebases.

S3: 00:21:26.234 But it's good also-- we should maybe even do that during the course so that students are like, "Oh, I need to come back next week." That's a good thing.

S4: 00:21:34.593 Oh yeah, we can do spoilers like--

S2: 00:21:40.473 Cliffhangers.

S1: 00:21:44.059 So as we pause--

S5: 00:21:43.710 That actually has proven to work pretty well, like have the spoilers and recaps of the previous week. Yeah, it's like Netflix, so.

S1: 00:21:54.831 So all the verbs for activities that you said, Loic, are great and are different from reproducing the manual bug. But, Marc reproducing--

S4: 00:22:10.316 That's in bold so that's related to their project.

S1: 00:22:13.791 Sure, but as a prerequisite to reproduce the bug. So, is it what Loic said like download or clone the source code, somewhere have the source on your machine, have a test bench, have a development environment ready, that kind of stuff?

S4: 00:22:44.878 Mm-hmm.

S1: 00:22:45.635 All right. So it's not only reproduce the bug but get the development version and make it run on your machine, which is--

S4: 00:22:58.050 Yes. That was--

S1: 00:22:58.748 If we go as deep as that, that is huge.

S2: 00:23:05.156 No.

S4: 00:23:04.857 No, because we don't need to tell them the actual tools that we use, just how to find the information, and what this means.

S2: 00:23:14.476 Yeah. They will do actual things. But the course must not be specific to anything. Just say that the concepts are the same. But you mentioned the development environment. One thing that is also extremely important, is how do you make it so you have the development environment on your machine, without modifying your machine, that is using a virtual environment to do that or the cloud. And this may be also a prerequisite. Are you able to bootstrap whatever method, a new machine that is completely virgin.

S4: 00:24:02.321 VirtualBox.

S1: 00:24:03.761 VirtualBox.

S2: 00:24:04.347 It could be VirtualBox or Docker or whatever, but the goal is can you start an operating system that is blank and you work from that? Because if they don't, it will make it much more difficult to fix the bug or even install the environment because there will be things in their environment that interfere, a dependency that they cannot fix. Yeah. So maybe a prerequisite and an activity.

S1: 00:24:40.811 In many projects, I saw give Docker version to get all the development environment ready.

S2: 00:24:49.248 They have sometimes too. Yeah. Open edX for instance.

S1: 00:24:55.079 Oh, my gosh. Yes.

S2: 00:24:58.178 Yeah. Exactly. Xavier insisted on that. He loves Docker.

S3: 00:25:03.568 Yeah. No, I don't. But I mean, whether it's Docker, Vagrant, or whatever, even if they provide it, there's always a bunch of stuff to do. Even with all of that on Open edX, it's still one of the longest tasks for any new contributor is to set up the development environment. And there is definitely no way we could address that in a generic way in the course, by the way. It is so specific every time.

S1: 00:25:30.893 That means you don't have one Docker, one [peak?] and you get the development environment. It's much more difficult.

S3: 00:25:36.935 I mean, in theory, you do. The problem is that when you click on the button, something always fails or whatever, and you need to debug that and etc., so yeah.

S1: 00:25:51.892 Yes. I agree. So we have in deploying the environment. Yeah. I agree that many, many times when I first wanted to contribute, I couldn't make it run, the development environment.

S3: 00:26:15.112 And actually, I think that's maybe the part where we can bring the most help is that those situations are quite standard. There are some instructions on the site. It doesn't work because it's outdated or someone broke it and nobody cared. How do you deal with that? That becomes a social problem which I think is much more in scope what we do. Like you need to go talk to people. Don't stay stuck with the thing on your corner. How do you talk about it? Do you just say, "It doesn't work when I install"? No, you need to provide details about what doesn't work and etc. So I think those things would be important to pass on because it's always the same thing, always the same mistake that newcomers do. And if we give those advice we already pass like 90% of the blockers I think.

S1: 00:27:04.348 Yes.

S2: 00:27:05.527 I agree. And even since we're at the technical level and we don't want to dive into specifics for the project, this actually is a fallback activity. That is, you aim for a bug. If you fall into a bug in the installation instruction, then the bug is this, and you report it because it presumably does not exist and you fix it. And that's what I always do when I run into a project and there is difficulty. And as you say, it happens often. So it's not a blocker. It changes the goal of the week.

S1: 00:27:55.610 But then it's a bug in the documentation on how to--

S2: 00:28:00.303 It could be a bug in the documentation.

S4: 00:28:03.727 But not always.

S2: 00:28:03.808 A month ago I've run into a project where the bug was actually-- yeah, it was in the documentation, but it was in the script. In the documentation, you had to copy-paste three lines. And two of the lines were reverted. That was super simple. But then the fix was a fix of the documentation, but it took me a day to actually fix it and understand it and get them to acknowledge it. So it went from, okay, I fixed it. I don't tell anyone, and it stays there. And you have to accept that it's a fallback activity. And if in the MOOC it's written down as an acceptable fallback activity, then people will not overlook it, and it will be actually useful to the project.
[silence]

S4: 00:29:15.771 Yeah, we could. We could actually recommend that if some instructions do not work, yeah.

S2: 00:29:23.913 Oh, there is one other skill. A prerequisite, also. A concept. You have to understand code coverage; what does it mean? So you will run tests, and you will look for code coverage. And it's very simple to convey why is it a prerequisite because you fix one line. You have to verify with the code coverage that the line that you fixed is actually run by the test. Otherwise, you don't know.

S4: 00:30:01.401 That's for projects which have tests.

S5: 00:30:08.463 I have a question about--

S?: 00:30:09.591 [crosstalk].

S5: 00:30:12.045 --a question about the prerequisites, because it seems like now we're adding a lot of technical competencies people need to have. And I just wanted to check in with the personas that you defined for the MOOC, if it's still the same people because it seems like some of the people, the potential learners you had in mind, might not have these technical competencies. I'm looking at the page now, and there's the CEO of a company or just a person who's kind of interested in open source. So it seems like it might be some people who are not-- that have those technical skills that you're talking about, so.

S2: 00:30:53.233 Very good point. [inaudible] advanced tooling, how do you do that? If you want--

S4: 00:30:58.440 I think we could use this to explain those concepts and how they vary a lot across projects. Because I know very, very few projects that actually use the concept of code coverage. I mean some of them have a percentage on the GitHub page of saying, "Oh, we have tests that cover 60 or 70 percent," and don't really monitor that, or.

S2: 00:31:39.867 True, true. But the point is--

S4: 00:31:43.502 But you can explain the concept, yeah.

S2: 00:31:44.613 --you have to understand that, otherwise it's-- you have to look for it. Because if it's there, it's the guarantee that your test is actually doing what you expect it to do instead of nothing.

S4: 00:31:58.642 Yeah, but I think it--

S2: 00:31:59.995 If you don't have it, you cannot create it.

S4: 00:32:01.399 It could be explained in a few minutes without having it a prerequisite.

S2: 00:32:10.710 Ana has a very good point. The CEO of a company has no clue what code coverage is. And it's not going to be easy to explain to someone who is-- unless he is or she is a tech person in the space.

S4: 00:32:33.674 Being a tech person is a prerequisite.

S2: 00:32:36.361 Then, how technical? I guess that's what I--

S4: 00:32:39.596 Knowing how to program technical. That was a prerequisite from the start, that people should know how to program.

S2: 00:32:47.899 Okay, now one could argue that if you know how to develop software, you know what code coverage is.

S1: 00:33:01.070 Maybe not.

S3: 00:33:01.560 I also understood your comment, Ana, as being broader. It's not just about code coverage. It's a lot of things on this section currently are very technical. So how do we handle people who are not interested to dig in to that? Do we make that whole section non-mandatory for them? Do we have just a summary of the beginning for the big headlines? How do you handle those personas?

S1: 00:33:30.459 The thing is there are some MOOCs on DevOps. So basically it's everything around DevOps, how to set up a dev environment. And there are so many tools. It's impossible to cover everything. Maybe the major ones, but.

S4: 00:33:53.045 Yeah. That's why I think what Xavier said earlier about don't explain the tools themselves but the concept of the tools and where to find the information and how to ask smart questions about the instructions and the tools that some projects use.

S5: 00:34:14.141 Yeah. I mean definitely, it shouldn't be a problem to refer people to some external resources if they need to. But I think it's just the question of time spent-- not necessarily time but the effort that they have to make in order to really dive into that. If on week five they suddenly realize that they have to take three other MOOCs in order to get through that week they will probably abandon here. Or at least they should, from the start, that week five might be three times longer for them if they're not-- I don't know, if they have never worked with this specific concept, for example.

S1: 00:34:55.532 There is so much varieties. So maybe it could be you know how to type something in the command line. And you just type, as you said Loic, three commands. And then you get the code and everything is running. Or if it is a more complex environment then you could be even stuck in a complicated process. It really depends on the project.

S4: 00:35:31.698 Yeah. But in any case, you should be able to ask for help at this point saying, "This instruction set is completely not understandable. Please help me with this. I'm stuck here." And, normally, projects should know that newcomers cannot understand the instructions and have some kind of external help, like saying, "Oh, you have problems with just running Django through Apache. So here is the document which we usually refer people to."

S1: 00:36:17.156 Another thing is, so you mentioned Apache, Django, all that kind of technologies. And indeed sometimes the documentation of a project has a lot of names of other projects. So you have to know what it is. You have to know what is Django. You have to know what is Apache if you don't know. So it's not only applying in a stupid manner what you see but also reading a lot. And then understanding the architecture of the project also.

S4: 00:36:55.271 Yeah, but that's a matter of choosing the open-source project you will contribute to, right? So it's--

S1: 00:37:02.679 Choosing and understanding the architecture of the project and how it is linked to many of--

S4: 00:37:08.516 So I won't to say that we would do it next week, but that it should have been thought of before. [laughter]

S1: 00:37:15.008 Okay. All right. So when you find the project you have-- yeah, I think I mentioned that before also. That you have to find all the important terms, libraries linked to other projects, or how the architecture. If it is NPM package, well, you have to know what is NPM then.

S5: 00:37:50.849 Do you think that maybe if it's such a-- if there is such a variability in terms of actions and knowledge that the learners need to take on this week, maybe there is a possibility to provide at least some sort of scaffolding or like a structure or steps that they need to take, I don't know, a checklist, something that will allow them to at least try that they're on the right path, so they won't get completely independent in working on this [week?]?

S1: 00:38:26.393 Yes.

S4: 00:38:33.590 So a checklist on how to build a project-- how to test a project in general from source?

S5: 00:38:40.777 No, no. Not necessarily a checklist. I'm just wondering just how to make sure that you don't lose people who are not 100% sure of what they're doing or who are getting lost. And they are searching for a bunch of information, and they're not even sure if they're looking for the right thing anymore. And, yeah, just this sort of uncertainty because if there's going to be a lot of information that they will have to go and search themselves, they at least need to know maybe that they're on the right track.

S2: 00:39:14.149 Just to make it clear what I have in mind in terms of steps throughout the week, there are not many, but they take some time. They require time to figure out, and they are simple. The time that is required is just because you have to do some exploration. So that would be get the code. So you have to explain things around getting the code, but it's not very ambiguous. It's fairly simple. And then you have to run the test. You know that in the end, you want the test to say, "Okay, it works."

S2: 00:39:57.873 So, again, I think it's fairly specific regardless of the project. And you always have that, or you don't have test. Okay, but it's crossed. Okay, you did that. There is no test. Next step, you write a modification. So this is entirely on you. It could be fix a typo or change the code. So, again, it's fairly specific. In the end, you have a modified codebase. That's the end of this step.

S2: 00:40:34.770 And then you have the next step, which is send this modification to the project. Again, it's easy. You start from the modification that you did. The end of this step is, it is available on the project bug tracker as a possible fix for the bug. And then you wait for the review, you see. So it seems to me that it's not that's confusing, although there are many moving parts and although mileage varies a lot between projects. But this is always going to be this way. So people may get lost in between steps. The steps themselves, they are fairly self-contained, I feel.

S4: 00:41:33.796 So we wrote these steps in the [path?]. And basically I think that the steps that most relate to this week are the steps like build the code and run the tests. Because writing a modification is something where you need to know where stuff are and how to find where the stuff related to your bug report is, which is mostly what we will discuss next week. So in my opinion, get the code is something you should be able to do at week two. Like try to clone something, get locally your code. Build it and run the test is specifically something that we should, in my opinion, focus on here. And then write a modification requires you to understand the structure of the code and how to navigate it.

S2: 00:42:37.556 I disagree because in terms of tooling, you're only halfway. So I'm not saying fix the bug that you are assigned to fix. It could be look for a typo in the documentation anywhere. Fix that. So you fake it. What is interesting is what is going to be required of you in terms of tooling after you fixed because there is tooling there. Sending the patch is not always done in the same way. Waiting for the review, addressing the review is not always done in the same way. And there you finish the process until your-- so I would agree with that in this week you want to fix the typo in the documentation because it allows you to navigate all the contribution process in terms of tooling. And so when it comes to next week where you have to explore the base, you already know your way. And you can focus on the codebase, your fix. You don't have to worry about the tooling. You know that you will get there. Otherwise, you will discover the tooling that you need afterwards.
[silence]

S4: 00:44:05.830 So it could be a trivial fix like change the string in the interface and check that you can run it with the string modified.

S2: 00:44:15.736 Yeah. Or as I said, a typo. There always is a typo.

S4: 00:44:21.277 I don't put typos in my code.

S2: 00:44:24.795 Oh, sorry. Except your code, always there's typo. So it's actually what I do when I'm not familiar with the process. I look for a typo. I fix the typo. Just to navigate the entire process. It also tells you, and this is on the social thing, how fast the reviewer react or not. So you have a little more time to know that it will take two weeks for someone to look at your patch, or maybe it will take one day.

S1: 00:45:04.785 And, Marc, so you are one of the contributor of Inkscape just to get an idea of the codebase and how long it takes to build it.

S4: 00:45:22.152 One hour on one core. On one core. So divide it by your number of cores and you have typical compile time.

S1: 00:45:34.760 And what is the size of the codebase?

S4: 00:45:39.763 The size of the repo or the size of the codebase current? Because the repo is huge because it has a big history.

S1: 00:45:47.682 So that's another thing. Should you clone the whole repo or only the part that is relevant to what you want to work on?

S4: 00:45:59.651 No. You have to clone everything. But I meant if you want to speed up your clone you Git clone a single branch and depth equal 10 or something. But if you will need to work on the history, you will need a bigger depth. So that's for the download time that you will need, depending on your connection, no matter what. And you need all the current state to build it basically, and it's not that big.

S1: 00:46:34.318 Give me a number.

S4: 00:46:35.888 Yeah, one second.

S1: 00:46:42.714 And, Xavier, do you know the size of the codebase of Open edX?

S3: 00:46:49.355 I wouldn't be able to tell in number of lines. No, I don't have it on top of my mind. But I mean there is no compilation for the Open edX codebase, but the deployment or still gathering of assets and etc. takes a good hour, sometimes an hour and a half also, so you have something similar. I mean, it's a little bit faster with the development environment set up, but you also have a bunch of those types of needs also takes a while to bring it up.

S1: 00:47:27.706 It could be discouraging to put a line in the command line somewhere and hit enter and then wait for one hour. So they have to be prepared that sometimes projects are huge, codebases are huge. It takes a lot of time to get the code. It takes a lot of time to build it. So, yeah.

S4: 00:47:54.133 Well, they can do other stuff in the meantime like you run the compilations, and if you did not change everything and the CI has passed on the-- and you have the dependencies, there is a good chance that it will succeed, so you don't have to monitor it, and you can do something else and come back later.

S3: 00:48:17.945 Yeah. And how we've said in the past there are many steps that take time, like actually, the technical part like deployment, compilation, or even just running the test suite, which happens automatically on a pull request, is only a part of it because you have to wait for the reviewer. Actually for new contributors, the test suite doesn't run automatically. There needs to be-- someone needs to come on the pull request and activate it for security reasons. So even your first question in the chat might take days to be answered. So definitely working with the timeline and not expecting things to happen immediately is important. Clearly if students procrastinate the whole thing and on the last week the last few days they start taking care of things, that's never going to work. So we need to warn them.

S4: 00:49:11.010 Yeah. That is the [big?] for week zero. You cannot do it-- you cannot do everything at the last minute because that's just not how things work. Except if you already know the contents of the course and you pick things with a typo and you submit a merge request. You can do what the course asks in two hours if you already know the contents of the course.

S1: 00:49:39.241 What about the prerequisite in term of hardware and network connection?

S4: 00:49:46.945 You need a computer and you need internet access. And the rest will depend on the project you choose.

S3: 00:49:55.829 Yup, that is a good point because Open edX requires a lot of memory for example. So, yeah.

S2: 00:50:03.159 And also even for project that only require modest resources, I've seen students come in courses with laptops that were absolutely unfit even to edit text with a word processor. So I couldn't wrap my mind how did they expect exactly to achieve anything with that kind of machine. So it's a good point.

S4: 00:50:36.851 And you need some storage space.

S1: 00:50:45.012 Or some storage in the clouds.

S4: 00:50:48.733 You cannot build anything in the cloud.

S2: 00:50:52.583 Yes, you can.

S4: 00:50:54.689 Yes, you can, but.

S2: 00:50:56.970 And once I started to start the course. So on average, there were 20, 30 people. And I had five virtual machines set up ready to use for the people who didn't have any resource available. And that came handy, otherwise we would just lost them. They had to watch someone else do the work.

S1: 00:51:24.318 That's an interesting point. We could offer this kind of service, Marc, like a virtual machine to build everything. No, but I mean, even the student is paying for something. Paying for mentoring, paying to have this kind of access also?

S4: 00:51:42.644 Yeah, but either it's limited to things that work on the network like a website, like OpenStack or Open edX. But if you have to work on, for instance, Blender or Inkscape, you will need a graphical interface at some point. And so we could offer service like Stadia or Shadow or stuff like that with a Linux machine, and that would work. But I don't see this MOOC as a Stadia competitor. I mean, we could make it so. But it would be, I don't know, maybe a bit too big. I don't know what's possible. And I don't know if any company's already offering a virtual machine with graphical for development on the cloud. And probably there are some, and I don't know what that costs.

S2: 00:52:57.988 But even if it's true, when you say Blender, yeah, it's a non-starter because it will not be able to do 3D. And you will not be able to see it in real time. So that's more clearly [inaudible], so.

S4: 00:53:10.123 Well, if you have a Shadow machine, you will have a graphical card deported, and you can probably run Blender and do stuff like [inaudible]. It will be expensive.

S1: 00:53:23.585 Oh, yeah. I pay €12 a month to have access to a very good computer with a huge graphic card. But anyway, I was thinking about debugging tools. So do we speak about that?

S4: 00:53:48.044 I think that's the same as for building it. It's so language-specific that you have to ask people what questions and what information they should be looking for rather than try to focus on GDB or LLDB or I don't really know what tools are more used for Python, for Open edX, for OpenStack, or.

S2: 00:54:15.249 It's a good point because as the culture of tests grew, the culture of debuggers shrank. They still have debuggers, and they are still the only way you can fix something in a C or a C++ codebase or figure out something, but it became increasingly rare. So, yeah. It's absolutely worth mentioning. It shouldn't be a prerequisite, I think, the ability, because, in the end, it's more likely that you will fix the bug by working on the test, running the test, seeing the result, rather than introspection during the execution of the code. Yeah.

S1: 00:55:03.289 Well, I was working a lot with JavaScript projects, and it's a mix of test and debug, but.

S2: 00:55:20.864 Sure. Sorry. I'm not saying it's disappearing. I'm saying it tends to be dominated by tests. Nowadays it's rare to find a software that has no test, and it's very common to find a software where figuring out how to step in the debugger is really difficult.

S1: 00:55:48.744 Interesting. So one very important concept I see here is the test concept, as you mentioned, Loic, and also maybe the debug concept. So do you mean that test-- so it means like test-driven approaches, something like that is more common?

S2: 00:56:21.472 It's a matter of taste. You have your mind set up in a way that you think about the test before thinking about the code or the concept, and some people do, others don't. I don't. But I understand other people, that their mind work in different ways. But testing is deep down, and I would love for a course to have that in the content. I think I've never seen that written anywhere that tests are to software what a demonstration is to a theorem because you cannot conceive a software without a test. There is no proof. In the same way, you cannot say I found a wonderful mathematical theorem, but I don't have a demonstration. That just does not work.

S1: 00:57:25.088 I think if you want to prove a software then you have to verify it. It would be more verification tools.

S2: 00:57:33.035 But it's a test. It's exactly that. [crosstalk].

S4: 00:57:35.420 No, no, no. Formal verification is much, much more powerful than tests.

S1: 00:57:41.788 Right.

S2: 00:57:41.938 Yeah. Okay.

S4: 00:57:43.382 Like Coq or Isabelle or whatever.

S2: 00:57:46.429 You could have that. But both of them verify software. One is more powerful than the other. But their goal is the same: verify the software. And if you don't have tests, you don't know the software works. That's the end of it.

S1: 00:58:00.344 At least, yes. Testing is the very basic stuff, yeah, yeah, of verifying if you wish.

S4: 00:58:12.996 Okay. Now I'm thinking about the number of tests in Inkscape, and I'm a bit ashamed, but. [laughter] We might have 10.

S2: 00:58:25.784 But then you can have humans acting as tests.

S4: 00:58:29.899 Oh yeah, of course.

S2: 00:58:29.316 It's super expensive, super not reliable, but some projects, such as the Linux project, they entirely rely on human-driven tests.

S4: 00:58:43.485 Okay, that works. It may be more applicable to end-user programs rather than architectural stuff? I don't know, UI testing is almost impossible to do automatically, so.

S2: 00:59:04.980 I disagree completely, but we are not going to go there, otherwise, it will be another 10 hours.

S4: 00:59:09.559 Yeah, yeah, yeah, of course.

S2: 00:59:14.577 Yeah, acceptance tests in UI and UX has changed everything.

S4: 00:59:23.823 Okay, I would be interested to hear about that, but maybe not tonight. [laughter] I could invite you to an Inkscape developer meeting if you want to talk about that. [laughter]

S2: 00:59:35.703 Okay.

S3: 00:59:38.208 Oh, I need to come with some popcorn for that, huh?

S1: 00:59:43.927 [laughter] Well, looking to all we said now, I'm really wondering how to evaluate what will be done during this section. That is really something [inaudible] for me.

S4: 01:00:02.643 We could do peer evaluation?

S2: 01:00:06.073 I think that's the only thing you can do.

S4: 01:00:09.913 But that would mean we would need to have a cohort basically around the same, at least the same, world. Because you cannot peer evaluate someone doing stuff on Django by someone who is compiling C++ for Blender and has no idea what the other person has been doing.

S1: 01:00:30.742 That's mission impossible because you have to define what you [call walls?]. I don't know what it is, but.

S4: 01:00:36.608 Well, programming languages and sort of web versus desktop versus--

S1: 01:00:45.483 Mobile?

S4: 01:00:45.932 --mobile, oh yeah.

S2: 01:00:47.489 There is proof, and there is the quality of the proof. But the proof is, for instance, is there a URL in the project that you authored and that carries your patch? So this is the proof. Now, is the patch total crap? That is quality. And let's say, peer evaluation will not go there because it's very project-specific, but the reviewers will go there. So when this patch is merged then you have another proof, and that's not project-specific. It's merged. It's in the codebase. You can have a link that shows it's your code. It's in the codebase. That's super easy to verify. And I think that's you can do that peer review based on URLs, public URLs, that demonstrate that you did what you're supposed to do.

S4: 01:01:59.081 But you're not supposed--

S5: 01:01:59.878 What if they did something they're supposed to do, and it wasn't reviewed or nobody got back in touch with them, even though it did good work? [And that happened?]?

S2: 01:02:08.850 They failed. They failed.

S5: 01:02:11.483 Okay, they failed.

S2: 01:02:13.800 Yeah.

S5: 01:02:14.982 That's harsh.

S2: 01:02:15.378 And it's rough, but it probably means that they picked the wrong project. So it may not be-- it's negative note on this technical week that does not relate to technical stuff. But yeah.

S4: 01:02:37.143 I'm not sure how we can integrate the time concepts of-- the project may take like normally a week to answer a merge request, even for a typo. And that would not be a fail in that case. And I don't know if for peer evaluations you can choose to only evaluate things that happened a week ago?

S1: 01:03:08.135 No. Peer evaluations, Xavier stop me if I am wrong, but it's really not basic, but you cannot configure this kind of thing.

S3: 01:03:21.654 Right. What do you want to configure, exactly? I did not get that.

S4: 01:03:25.838 You submit your work, and you have to wait for a week before someone else gets to evaluate it.

S3: 01:03:35.806 I think you can have-- I mean, we need to double-check that, but I think you can have deadlines as to when the evaluation starts. You don't think so, Remi?

S1: 01:03:52.014 So from the point in time where you submit your work to the point in time where the reviews start, I think this is--

S3: 01:04:06.231 I mean, I would need to check. It depends unless you mean the relative dates because I thought you could have a submission deadline.

S1: 01:04:18.523 Sure. But you cannot force the-- because there is an automatic algorithm that--

S3: 01:04:26.378 That matches.

S1: 01:04:27.817 Yeah, that matches students. And when the match is okay, then you can start right away the reviews, right?

S3: 01:04:37.840 That's true. Well, what you could do without any code change is to ask students to submit by a certain date and have the actual submission deadline [in the two?] to be later. Obviously, some people will do it later, but at least-- yeah. You could also have a code change. Of course, that's work, so that's a different matter. But yeah, I see what you mean.

S4: 01:05:01.053 I think we could make a peer-review module with our needs.

S3: 01:05:13.888 Yeah. I mean, probably [recording?] completely a peer review module might be a little bit of work. We could always contribute changes if there are really some options that we are missing. At the same time, every time we want to make a code change, it's like a significant endeavor that takes over from the work on the course itself. So as much as possible, I think we should try to use the way we have it now. But obviously, if there is something that is really crucial to the course, we could consider it.

S1: 01:05:48.760 We are talking more about the last section, right?

S4: 01:05:56.907 No. I think Loic wanted people to commit something in this week. Like the trivial fix. Like a typo, or.

S1: 01:06:09.253 So a typo in the code.

S3: 01:06:10.733 Yeah. I mean, as I've said before, I would actually recommend to do that even earlier personally. I don't think we can do those things early enough, to be honest. So I definitely support at least doing it [at that point?].

S1: 01:06:27.363 We were talking about committing something in the documentation or something way before. Okay.

S4: 01:06:36.891 Yup. I'm a bit scared about asking people to commit trivial fixes, but.

S2: 01:06:45.955 [And why is it?]? But in every codebase, you have the documentation part or even comments or can be--

S4: 01:06:57.277 Okay, but do we advise people on how to find typos, or?

S2: 01:07:05.796 I mean, you read.

S4: 01:07:08.611 Yeah, but what I [inaudible]--

S2: 01:07:15.236 Go on your codebase, and tell me how long it takes you to find the typo by reading what is in there. Start one file after the other and tell me how long it takes. I bet it's less than one hour.

S3: 01:07:38.959 And especially if we include the initials, the process, and etc. People will go over those documentation, those steps, and etc. So yeah.

S1: 01:07:52.167 How do you prove what's-- so how do you-- is there a way to prove that you have cloned, that you have built the project, that you have development environment running [crosstalk]--

S4: 01:08:07.342 You can submit a screenshot.

S2: 01:08:10.860 Yeah, exactly. A screenshot. It's not really a proof or anything, but it would be stupid for someone-- it's so easy, so yeah. I guess a screenshot [inaudible].

S1: 01:08:19.682 Maybe the peer grading assignment would be to take a screenshot of the codebase that you have locally. But then how do you review that? You have to have unambiguous--

S2: 01:08:40.648 Again, I think you don't look at the quality of those thing. You look at the screenshot. Maybe it would be required to submit two things: a link to the codebase and a screenshot of the codebase on your machine. And so your review would be to go to the link and see that there is one file that has the same name, and that's it, and it's okay. And as you very well know, most of the time, students, the real challenge is just to do the assignment, no matter how simple. And some of them will not even do that. So that's not worthless.

S1: 01:09:30.057 Yeah, but it's not only doing that but also reviewing other's screenshots and stuff.

S2: 01:09:36.775 Yeah, and it's also-- yeah, it's graded too, I guess. Is it graded, the fact that you review other people?

S1: 01:09:44.090 I think you cannot access your grade if you have not graded others.

S5: 01:09:48.697 Yeah, it's part of your evaluation. You have to evaluate others [inaudible].

S3: 01:09:55.677 And also, if we include virtual environments, there could also potentially be automate checks, like is there a Git clone in the history of the shell or something? I don't know. I mean, obviously, it's tricky to do it for all projects if they can choose freely. That might be the tricky bit, though.

S1: 01:10:26.018 And something completely different is mobile development. So some of the Android or even iPhone, iPad, I don't know what, projects are open source. So do we consider mobile development also? So in that case, the hardware requirements are-- and even if you cannot have Android phone to debug or to test the project then it is definitely impossible because it's so difficult to have a virtual environment to emulate Android.
[silence]

S2: 01:11:24.922 That's a very good question, actually.

S4: 01:11:28.374 I probably would not recommend going into mobile development. But if that's what the students learns, then it's up to them to know the tools, basically.

S1: 01:11:45.942 I'm pretty sure you say that because you hate Java.

S4: 01:11:50.461 No.

S1: 01:11:51.370 Yes. Yes, you do. [laughter]

S3: 01:11:56.376 I guess that's a reminder of the fact that we really can't go too far in terms of explanations about the stack and etc. because actually, I'm sure that will have a diversity even bigger than we imagine. Like some people might go on different architecture, different types of projects, and etc. So it's probably good to keep that diversity in mind while writing the content.

S1: 01:12:30.468 Oh, that reminds me. Do you know StackShare? It's a website where many companies are sharing the stack of all the tools they use to communicate, to develop, to test, to debug, all the tools, and you can compare the stacks of many companies like this. So I find the-- I don't think it's open source, but anyway, StackShare. Let me give you the link, stackshare.io. And then they have categories of-- in our company, we use these tools to communicate, these tools to develop, these tools to deploy, etc., etc.
[silence]

S1: 01:13:35.005 And sometimes it makes a link on-- if you want to test a web application and the usual stack is Apache with MySQL with etc., etc. So it gives you standards on some form-- yeah, standard stacks, I would say.

S2: 01:13:56.935 I looked at it a few years ago, and I found the idea intriguing. But I don't see the actual value. I was intrigued, but.

S4: 01:14:14.678 Cannot look at things without logging in, so.

S1: 01:14:17.705 Actually, it captures the complexity of the variety and the different kind of stacks you can have just to get an idea of how diverse the tools can be.

S2: 01:14:35.334 Yeah. Oh, yeah. That's a good way to look at it. Now, I cannot go there because it requires a Google account or a GitHub account, none of which I have.

S4: 01:14:49.230 Same. I'm trying out to find out how to disable the [inaudible].

S2: 01:14:56.017 [laughter] You're trying to cheat.

S1: 01:15:04.699 Ah, indeed, this is new, right? They have a popup, and to see the stack you have to log in. It wasn't the case before. You can log in with GitLab if you want, GitLab, Bitbucket, Twitter, GitHub, Google, SSO.
[silence]

S4: 01:15:38.567 So we mentioned testing. We mentioned code review. We mentioned code coverage, tests. So we are basically adding in activities to send a trivial patch to any project basically. It doesn't have to be the one you're trying to contribute to, right, Loic? You're mute. You're muted.

S2: 01:16:37.404 Sorry, I was muted and into StackShare. I didn't hear you. Could you repeat, please?

S4: 01:16:45.091 Yeah. So we are moving basically the main activity of this week to commit a typo fix to anywhere basically. It doesn't have to be the same project you want to contribute to in the long-term, or.

S1: 01:17:05.355 And also prove that you have cloned the source code, and maybe we could do [crosstalk].

S4: 01:17:10.973 Oh, yeah, yeah. No. That's two activities, right? What is evaluated, and--

S2: 01:17:17.599 Yeah, the goal would be that. Yeah. Yeah, it's interesting that-- so you're saying that for that week five you could choose a different project from what you're after.

S4: 01:17:34.100 Yeah. I mean if you find a typo in the same project, then fine. But if you find another project with many typos and you find it easier-- or someone in the forum shares that, "Oh, I found this project we--" so the main thing that it doesn't know how to spell and it welcomes--

S2: 01:17:53.788 Yeah, that's interesting because what is evaluated there is the ability to understand the flow in technical terms, and it does not need to be the same project, too. So you will have to do the same for the project you're after.

S4: 01:18:13.901 Of course.

S2: 01:18:14.872 So it may be more work, but it may also be less work if the contribution is easier. Very good point.

S4: 01:18:23.834 And so we have two things. We have the screenshot stuff and we have the merge-request stuff. Are both evaluated, or are some of them, not optional, but not graded? What would make more sense?
[silence]

S2: 01:19:06.327 There would be also running the test. So it's useless for a typo, the tool. We don't really care because it's about technical tools. So running the test it could be also a screenshot of the output of the test on your local machine or your cloud machine.

S4: 01:19:23.842 Or it could be a non-evaluated and non-shared activity like please do that, and if you don't, then you're on your own.

S2: 01:19:33.054 Yeah, of course. Yeah.

S4: 01:19:42.709 So, Remi, what do you think about two peer-reviewed stuff or three?

S1: 01:19:56.423 I'm not really convinced [because?] peer review is cumbersome. It is difficult to make it work. It is slow. So two of them may be too much. Or do you mean in section five or one in section two and one in five, or?
[silence]

S4: 01:20:49.448 Sorry, I mute. I mean two in sections five, one with proving you've made a merge request for a typo and one with a screenshot that you've run the code. Or I'm thinking can we combine them in one. Or we have to put a screenshot or make a merge request for a fix. I don't know.

S1: 01:21:21.175 Or maybe the ungraded activity could be to put the screenshot somewhere and get the link of that somewhere without having it to be peer-graded. And everybody can see the links and stuff, and the graded one will be peer-graded.

S5: 01:21:46.964 By graded you are talking about the one with the four people with a certificate, right?

S1: 01:21:54.308 Yes.

S5: 01:21:54.686 So there is a potential danger of not having enough people to do that always especially if it's a certified part. So yeah, just something to be vigilant about.

S1: 01:22:11.466 Another is so to bootstrap if you don't have enough people in the evaluated track. There is an option, so that we grade instead of the students altogether.

S5: 01:22:34.595 Or teaching assistants, if you are thinking about having like teaching assistants or community mentors, which is also an option. But it will be an option after the MOOC started running already. It won't be an option for the first cycle.

S1: 01:22:53.329 That's another thing to consider is that graded parts could be also graded by the mentors.

S2: 01:23:04.887 Good people, I have to leave you because--

S4: 01:23:10.081 Oh, it's 8:00.

S2: 01:23:11.626 --food is waiting.

S5: 01:23:14.072 I was waiting for someone that's nice. [laughter]

S1: 01:23:17.825 I think we can we could stop there.

S4: 01:23:20.214 Yeah, we can. I think we mentioned most of the stuff that was in line with this week.

S1: 01:23:27.110 Yes.

S4: 01:23:29.908 So, yep. Do you see other steps that will need to be discussed in this tool module, or? Do we provide resources like external resources for [inaudible] or languages, or?

S1: 01:23:57.420 Sure. And I'm pretty sure we'll find a lot of resources for all that. I guess there's so many that it could be impossible to list all them.

S4: 01:24:14.123 Yeah, but I mean, do we try to? Or do we just tell people to ask the project they contribute to and ask them? Or do we crowdsource it into a wiki or a forum?

S1: 01:24:28.040 Yes, yes. We should crowdsource it. Yes. I really like this idea.

S4: 01:24:32.364 But in the MOOC itself or in the forum, or?

S1: 01:24:35.523 In the MOOC.

S4: 01:24:37.531 Okay. So we ask people to add things to the MOOC, if the project uses something in particular, or?

S1: 01:24:44.782 Yeah. Sure.

S5: 01:24:46.296 Maybe you can start by adding some resources that seem the most obvious. You need to kind of set the tone and then people will add more.

S4: 01:24:57.033 But it would make for basically optional content that people would look at only if they were interested in the specific language, or?

S1: 01:25:09.329 Yes.

S5: 01:25:10.975 But from what I'm understanding, there will be at least-- so everyone will have to go and look at least at something. So some of it will be optional, but there will always be one relevant resource for everyone. So it's kind of half-half, right?

S4: 01:25:30.758 It would probably end up by being 99% of stuff you will not be interested in, and some specific stuff will matter for the project you contribute to. But it could make sense, yeah.

S5: 01:25:54.438 All right. I am still wondering about the time of learning, and if it's possible to evaluate it somehow, especially for the weeks that are more practical and that are very much dependent on the external factors because, yeah, it would be nice to have a possibility to give at least some approximate time that these weeks will take. Just because people will plan their time accordingly, especially MOOC learners that would like to know probably how much time they're looking for.

S1: 01:26:33.831 Good point. And it could be different depending on your profile. So if you know more about Git and stuff, maybe you will go fast here. So, yeah, very good point. So we will make the effort of evaluating the effort in terms of time for each module week.

S5: 01:27:04.470 Yeah. Okay.

S1: 01:27:06.413 All right. Well, thank you all for joining this session again. And I hope to see you to the next one.

S4: 01:27:14.201 Yep. Thank you, everyone.

S5: 01:27:15.276 [inaudible].

S3: 01:27:16.502 Yep. Thank you.

S5: 01:27:19.041 Thank you.

S3: 01:27:19.422 See you next week.

S4: 01:27:19.626 See you next week. Bye-bye.
